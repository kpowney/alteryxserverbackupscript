This project contains a script for backing up an Alteryx Server instance. 


For more information please see the following articles:

Alteryx Server Backup & Recovery Part 1: Best Practices - https://community.alteryx.com/t5/Alteryx-Knowledge-Base/Alteryx-Server-Backup-amp-Recovery-Part-1-Best-Practices/ta-p/22620/

Alteryx Server Backup and Recovery Part 2: Procedures - https://community.alteryx.com/t5/Alteryx-Server-Knowledge-Base/Alteryx-Server-Backup-and-Recovery-Part-2-Procedures/tac-p/749436#M835


Version History:

2.0.2
- Set new variable for timezone
- Improved logging to include timezone information and provide additional clarity

2.0
- Added service wait timeout variable
- New logic to check service state when stopping or starting the service
- Error catch if service fails to start or stop as expected

1.5
- Added additional variables to make the script more robust. 
- Changed service start/stop method. 
- Revised various commands to use new variables

1.0
- Initial version
